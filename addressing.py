import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as signal
import scipy.ndimage as ndimage
import argparse
import json
import os


def cut_img_rect(img, rect=(None, None, None, None)):
    return img[rect[0]:rect[1], rect[2]:rect[3]]


def peaks_pos_to_binary(peaks, length):
    p = np.zeros(length)
    p[peaks] = 1
    return p


def peaks_d_hist(img, axis, threshold=0):
    peaks = detect_peaks(img, axis, threshold)
    peaks_d = [b - a for b, a in zip(peaks[1:], peaks)]
    plt.hist(peaks_d, bins=max(peaks_d))


def detect_peaks(img, axis, threshold, height=2, distance=15):
    mean = np.mean(img, axis=axis)
    mean = [x if x > threshold else 0 for x in mean]
    mean = signal.medfilt(mean, 5)
    peaks, _ = signal.find_peaks(mean, height=height, distance=distance, plateau_size=(0, 5))
    return peaks


def borders(img, axis, min_gap=16, max_gap=22, threshold=1.5):
    peaks = detect_peaks(img, axis=axis, threshold=threshold, height=2, distance=min_gap)
    peaks_d = [b - a for b, a in zip(peaks[1:], peaks)]

    peaks_d = [p for p in peaks_d if min_gap <= p <= max_gap]
    d_med = np.median(peaks_d)
    if len(peaks) < 2:
        raise ValueError("Only 1 peak detected, try moderate parameters.")

    peaks_final = []
    i = 0
    while i < len(peaks) - 1:
        if peaks[i+1] - peaks[i] > min_gap + d_med:
            peaks_final.append(peaks[i])
            pos = list(np.arange(peaks[i], peaks[i+1]-min_gap, d_med).astype(np.int))
            peaks_final += pos[1:]
            i += 1
            continue
        peaks_final.append(peaks[i])
        i += 1
    if i < len(peaks):
        peaks_final.append(peaks[-1])

    peaks_final.append(peaks_final[-1] + d_med)

    return np.round(np.array(peaks_final) - d_med / 2).astype(np.int32)


def subgrids_borders(img, axis, min_gap, prominence=5):
    mean = np.mean(img, axis=axis)
    mean = 1 - ndimage.maximum_filter1d(mean, 51)
    peaks, prop = signal.find_peaks(mean, prominence=(prominence, None), width=(None, None), distance=min_gap)
    if len(peaks) < 2:
        raise ValueError("Only 1 peak detected, try moderate parameters (prominence, min_gap")
    peaks_d_med = np.median(np.array(peaks[1:]) - np.array(peaks[:-1])).astype(int)
    peaks = [peaks[0] - peaks_d_med] + list(peaks) + [peaks[-1] + peaks_d_med]
    return peaks


def rects_from_borders(x_borders, y_borders):
    rects = []
    for i in range(len(x_borders) - 1):
        for j in range(len(y_borders) - 1):
            rects.append((x_borders[i], x_borders[i+1], y_borders[j], y_borders[j+1]))
    return rects


def load_image(path, rect, flag):
    img = cv2.imread(path, flags=flag)
    img = cut_img_rect(img, rect)
    return img


def address_microarray(img, desired_subgrids, desired_spots, **kwargs):
    prominence = kwargs.get('sectors_peak_prominence', 5)
    subgrid_distance = kwargs.get('sectors_distance', 400)

    spot_min_gap = kwargs.get('min_spots_gap', 16)
    spot_max_gap = kwargs.get('max_spots_gap', 22)

    x_subgrids = subgrids_borders(img, 1, subgrid_distance, prominence=prominence)
    y_subgrids = subgrids_borders(img, 0, subgrid_distance, prominence=prominence)


    print(F"Detected subgrids: {len(y_subgrids)-1}x{len(x_subgrids)-1}")
    if desired_subgrids[0] == len(x_subgrids) - 1 and desired_subgrids[1] == len(y_subgrids) - 1:
        print(F"Subgrids are detected properly (probably)")
    else:
        print(F"Subgrids count and desired subgrids count differs")
        print(F"Desired subgrids: {desired_subgrids[0]}x{desired_subgrids[1]}")

    result = dict()

    for i in range(len(x_subgrids) - 1):
        for j in range(len(y_subgrids) - 1):
            sector_id = (i, j)
            rect = (x_subgrids[i], x_subgrids[i+1], y_subgrids[j], y_subgrids[j+1])
            sector = cut_img_rect(img, rect)
            x_borders = borders(sector, 1, min_gap=spot_min_gap, max_gap=spot_max_gap, threshold=0)
            y_borders = borders(sector, 0, min_gap=spot_min_gap, max_gap=spot_max_gap, threshold=0)
            detected_spots = (len(y_borders) - 1, len(x_borders) - 1)
            valid = detected_spots[0] == desired_spots[0] and detected_spots[1] == desired_spots[1]
            if not valid:
                print(F'Desired number of spots differs from number of detected spots: '
                      F'{detected_spots[0]}x{detected_spots[1]} for sector {sector_id}')
            result[sector_id] = {'valid': valid, 'x_spot': x_borders, 'y_spot': y_borders, 'sector': rect}
    return result


def cut_spots(img, addressing_results: dict, only_valid: bool = True):
    spots = dict()
    for key, item in addressing_results.items():
        if only_valid and not item['valid']:
            continue
        spots[key] = dict()
        x_borders = item['x_spot']
        y_borders = item['y_spot']
        x_sector = item['sector'][0]
        y_sector = item['sector'][2]
        for i in range(len(x_borders) - 1):
            for j in range(len(y_borders) - 1):
                spot_id = (i, j)
                rect = (x_sector + x_borders[i], x_sector + x_borders[i + 1],
                        y_sector + y_borders[j], y_sector + y_borders[j + 1])
                spots[key][spot_id] = cut_img_rect(img, rect)
    return spots


def save_spots(spots_dict, out_dir):
    for sector_key, sector in spots_dict.items():
        for spot_key, spot in sector.items():
            spot_address = F"{sector_key[0]}_{sector_key[1]}_{spot_key[0]}_{spot_key[1]}.tif"
            path = os.path.join(out_dir, spot_address)
            cv2.imwrite(path, spot)


def mark_spots(img, addressing_result):
    for item in addressing_result.values():
        rect = item['sector']
        x_borders = [x + rect[0] for x in item['x_spot']]
        y_borders = [y + rect[2] for y in item['y_spot']]
        img[x_borders, rect[2]:rect[3]] = 255
        img[rect[0]:rect[1], y_borders] = 255


def print_validity_map(addressing_result, desired_sectors):
    a = np.zeros((desired_sectors[1], desired_sectors[0]))
    for y in range(desired_sectors[1]):
        for x in range(desired_sectors[0]):
            a[y, x] = 1 if addressing_result[(y, x)]['valid'] else 0
    plt.imshow(a)
    plt.show()
    print(F'Valid sectors: {np.sum(a)}/{np.prod(desired_sectors)}')


def addressing_main(img_path, config_path, mode, only_valid, out_path=None):
    config = json.load(open(config_path))

    array_structure = config['array_structure']

    desired_sectors = array_structure['sectors']
    desired_spots = array_structure['spots']

    addressing_params = config['addressing_params']
    array_rect = array_structure.get('array_rect', [None, None, None, None])

    img = load_image(img_path, array_rect, 0)
    addressing_result = address_microarray(img, desired_subgrids=desired_sectors, desired_spots=desired_spots,
                                           **addressing_params)

    if mode == 'mark_spots':
        mark_spots(img, addressing_result)
        cv2.imwrite(out_path + '.tif', img)
    elif mode == 'save_spots':
        img = load_image(img_path, array_rect, -1)
        spots = cut_spots(img, addressing_result, only_valid=only_valid)
        save_spots(spots, out_path)
    elif mode == 'validity_map':
        print_validity_map(addressing_result, desired_sectors)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--path', required=True)
    parser.add_argument('--addressing_config', required=True)
    parser.add_argument('--out_path', default=None, required=False)
    parser.add_argument('--mode', choices=['mark_spots', 'save_spots', 'validity_map'])
    parser.add_argument('--only_valid', default=True, required=False)

    opt = parser.parse_args()
    addressing_main(opt.path, opt.addressing_config, opt.mode, opt.only_valid, out_path=opt.out_path)
